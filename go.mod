module gitlab.com/pokstad1/protoc-gen-gitaly

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.2.1-0.20190205222052-c823c79ea157
	github.com/stretchr/testify v1.3.0
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
)
