
pb/optype.pb.go: pb/optype.proto
	protoc --go_out=. pb/optype.proto

testdata/%.pb.go: testdata/%.proto pb/optype.pb.go
	protoc --go_out=./testdata --proto_path=$(shell pwd)/pb -I$(shell pwd)/testdata $(shell pwd)/testdata/*.proto
